<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname' => str_random(10),
            'lname' => str_random(10),
            'email' => "muster@example.com",
            'password' => bcrypt('123456'),
            'company' => 'Muster Firma',
            'working_area' => 'Webentwicklung',
            'start_date' => '2017.01.08'
        ]);
    }
}
