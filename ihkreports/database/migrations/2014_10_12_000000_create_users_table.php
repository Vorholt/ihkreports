<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->string('password');
            $table->rememberToken();
            $table->string('company')->default(null);
            $table->string('working_area')->default(null);
            $table->date('start_date')->default('1970-01-01 00:00:00');
            $table->timestamps();
        });

        //Test DATA
        DB::table('users')->insert([
            'fname'        => 'Lennart',
            'lname'        => 'Vorholt',
            'email'        => 'lev@laudert.com',
            'password'     => '$2y$10$QA92cC03Emsn3q547cv7kuGj4tF9ZOpMcclqF8yxLgLI/5RD.aAUC',
            'company'      => 'Laudert',
            'working_area' => 'E-Business',
            'start_date'   => '2017-08-01 00:00:00'
        ]);

        DB::table('users')->insert([
            'fname'        => 'Max',
            'lname'        => 'Nelkner',
            'email'        => 'test@test.com',
            'password'     => '$2y$10$QA92cC03Emsn3q547cv7kuGj4tF9ZOpMcclqF8yxLgLI/5RD.aAUC',
            'company'      => 'BusinessUnicorns',
            'working_area' => 'Frontend',
            'start_date'   => '2019-02-01 00:00:00'
        ]);

        DB::table('users')->insert([
            'fname'        => 'Marco',
            'lname'        => 'Kappenberg',
            'email'        => 'kappenberg.marco@web.de',
            'password'     => '$2y$10$QA92cC03Emsn3q547cv7kuGj4tF9ZOpMcclqF8yxLgLI/5RD.aAUC',
            'company'      => 'BeispielFirma123',
            'working_area' => 'IT',
            'start_date'   => '2017-08-01 00:00:00'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
