<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('colourHex')->default('#FFFFFF');
            $table->timestamps();
        });

        // Test DATA
        DB::table('status')->insert(['colourHex' => '#FF0000', 'name' => 'Red']);
        DB::table('status')->insert(['colourHex' => '#FFFF00', 'name' => 'Yellow']);
        DB::table('status')->insert(['colourHex' => '#00FF00', 'name' => 'Green']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
