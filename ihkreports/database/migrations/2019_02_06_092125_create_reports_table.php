<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->default('0');
            $table->integer('number')->default('0');
            $table->date('weekstart')->default('1970-01-01 00:00:00');
            $table->date('weekend')->default('1970-01-01 00:00:00');
            $table->string('content1')->default('');
            $table->string('content2')->default('');
            $table->string('content3')->default('');
            $table->date('exported')->default('1970-01-01 00:00:00');
            $table->integer('statusid')->default('0');
            $table->timestamps();
        });


        //Test DATA
        DB::table('report')->insert([
                'userid'    => '1',
                'number'    => '1',
                'weekstart' => '2017-08-01 00:00:00',
                'weekend'   => '2017-08-04 00:00:00',
                'content1'  => 'Einrichtung PC',
                'content2'  => 'Firmen rundgang',
                'content3'  => 'Einschulung',
                'exported'  => '1970-01-01 00:00:00',
                'statusid' => '1'
            ]
        );

        DB::table('report')->insert([
                'userid'    => '1',
                'number'    => '2',
                'weekstart' => '2017-08-07 00:00:00',
                'weekend'   => '2017-08-11 00:00:00',
                'content1'  => 'Einrichtung PHP',
                'content2'  => '',
                'content3'  => 'kennelernen',
                'exported'  => '1970-01-01 00:00:00',
                'statusid' => '2'
            ]
        );

        DB::table('report')->insert([
                'userid'    => '1',
                'number'    => '3',
                'weekstart' => '2017-08-14 00:00:00',
                'weekend'   => '2017-08-18 00:00:00',
                'content1'  => 'Aufgaben PHP',
                'content2'  => '',
                'content3'  => 'Einführung C#',
                'exported'  => '1970-01-01 00:00:00',
                'statusid' => '3'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report');
    }
}
