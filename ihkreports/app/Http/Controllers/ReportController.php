<?php

namespace App\Http\Controllers;

use App\Report;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    private const MONTHS = [
        1 => 'Januar',
        2 => 'Februar',
        3 => 'März',
        4 => 'April',
        5 => 'Mai',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'August',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Dezember'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $oRequest = $request->all();
        $aData = \GuzzleHttp\json_decode($oRequest['report']);
        $oReport = Report::where('userid', Auth::user()->id)->where('number', $aData->report_number)->first();
        $iStatusid = $oReport->statusid;
        if (empty($aData->content1) && empty($aData->content2) && empty($aData->content3)) {
            $iStatusid = 1;
        }
        if (($aData->content1 || $aData->content2 || $aData->content3) && ($aData->content1 !== $oReport->content1 || $aData->content2 !== $oReport->content2 || $aData->content3 !== $oReport->content3)) {
            $iStatusid = 2;
        }
        DB::update('update report set content1 = ?, content2 = ?, content3 = ?, statusid = ? where userid = ? AND number = ?', [$aData->content1, $aData->content2, $aData->content3, $iStatusid, Auth::user()->id, $aData->report_number]);
        return $this->getAllSidebarDataByUser();
    }


    /**
     * Returns all sidebar data for the active user as json
     *
     * @return string
     */
    public function getAllSidebarDataByUser() : string
    {
        $oUser = User::where('id', Auth::user()->id)->first();
        $aData = [];
        $sThisYear = date('Y');
        $sStartYear = date('Y', strtotime($oUser->start_date));
        for (; $sStartYear <= $sThisYear; $sStartYear++) {
            $aData[] = [$sStartYear => $this->_getSidebarDataByYear($oUser, $sStartYear)];
        }
        return \GuzzleHttp\json_encode($aData);
    }


    /**
     * Returns report data array of active user for one year by given year
     *
     * @param $oUser
     * @param $iYear
     * @return array
     */
    protected function _getSidebarDataByYear($oUser, $iYear) : array
    {
        $aReports = Report::where('weekstart', 'LIKE', $iYear . '%')->where('userid', $oUser->id)->orderBy('weekstart', 'asc')->get();
        $aYear = [];
        if (count($aReports) > 0) {
            $iActMonth = date('n', strtotime($aReports[0]->weekstart));
            $aYear[] = [$this::MONTHS[$iActMonth] => $this->_getSidebarDataByMonth($oUser, $iYear, date('m', strtotime($aReports[0]->weekstart)))];
            foreach ($aReports as $report) {
                $iTmpMonth = date('n', strtotime($report->weekstart));
                if ($iTmpMonth > $iActMonth) {
                    $iActMonth = $iTmpMonth;
                    $aYear[] = [$this::MONTHS[$iActMonth] => $this->_getSidebarDataByMonth($oUser, $iYear, date('m', strtotime($report->weekstart)))];
                }
            }
        }
        return $aYear;
    }


    /**
     * Returns report data array of active user for one month by given year and month
     *
     * @param $oUser
     * @param $iYear
     * @param $iMonth
     * @return array
     */
    protected function _getSidebarDataByMonth($oUser, $iYear, $iMonth) : array
    {
        $aReports = Report::where('weekstart', 'LIKE', $iYear . '-' . $iMonth . '-%')->where('userid', $oUser->id)->orderBy('weekstart', 'asc')->get();
        $aWeeks = [];
        foreach ($aReports as $report) {
            $sStatus = DB::select('select colourHex from status where id = :id', ['id' => $report->statusid]);
            $aWeeks[] = ['number' => $report->number, 'exported' => $sStatus[0]->colourHex, 'weekstart' => $report->weekstart, 'weekend' => $report->weekend];
        }
        return $aWeeks;
    }


    /**
     * Inserts reports data into the database if active user is missing reports
     *
     * @param $oUser
     * @param $iLastReportId
     * @param $iWeekCount
     */
    protected function _updateReportsByUser($oUser, $iLastReportId, $iWeekCount) : void
    {
        $oMaxReport = Report::where('userid', $oUser->id)->where('number', $iLastReportId)->first();
        $sWeekstart = strtotime($oMaxReport->weekstart);
        $sWeekend = strtotime($oMaxReport->weekend);
        while ($iLastReportId < $iWeekCount) {
            $iLastReportId++;
            $sWeekstart += strtotime('+1 week', 0);
            $sWeekend += strtotime('+1 week', 0);
            DB::insert('INSERT INTO report (userid, number, weekstart, weekend, statusid) VALUES (?, ?, ?, ?, ?)', [$oUser->id, $iLastReportId, date('Y-m-d', $sWeekstart), date('Y-m-d', $sWeekend), 1]);
        }
    }


    /**
     * Updates the active users reports
     * If reports are missing adds the missing reports to database
     * Gets called in HomeController->index() after login
     */
    public function updateMissingUserReports() : void
    {
        if (Auth::check()) {
            $oUser = User::where('id', Auth::user()->id)->first();
            $iReports = Report::where('userid', $oUser->id)->count();
            $sStartDate = $oUser->start_date;
            $iWeeks = $this->_getWeekCount($sStartDate);
            if ($iReports < 2) {
                $this->_setInitialReport($oUser, $sStartDate);
            }
            $iReports = Report::where('userid', $oUser->id)->count();
            $iLastReportId = Report::where('userid', $oUser->id)->max('number');
            if ($iReports < $iWeeks) {
                $this->_updateReportsByUser($oUser, $iLastReportId, $iWeeks);
            }
        }
    }


    /**
     * Sets initial 2 reports if active user has no reports
     *
     * @param $oUser
     * @param $sStartDate
     * @return void
     */
    protected function _setInitialReport($oUser, $sStartDate) : void
    {
        $iReports = Report::where('userid', $oUser->id)->count();
        if ($iReports < 2) {
            $iUnixDate = strtotime($sStartDate);
            $iWeekday = date('w', strtotime($sStartDate));
            if ($iWeekday !== 0 && $iWeekday !== 6) {
                $sWeekStart = $sStartDate;
                if ($iWeekday === 5) {
                    $sWeekEnd = $sStartDate;
                } else {
                    $sWeekEnd = date('Y-m-d', strtotime('next friday', $iUnixDate));
                }
            } else {
                $sWeekStart = date('Y-m-d', strtotime('next monday', $iUnixDate));
                $sWeekEnd = date('Y-m-d', strtotime('next friday', $iUnixDate));
            }
            DB::insert('INSERT INTO report (userid, number, weekstart, weekend, statusid) VALUES (?, ?, ?, ?, ?)', [$oUser->id, 1, $sWeekStart, $sWeekEnd, 1]);
            $sNextWeekStart = date('Y-m-d', strtotime('next monday', strtotime($sWeekStart)));
            $sNextWeekEnd = date('Y-m-d', strtotime('next friday', strtotime($sWeekEnd)));
            DB::insert('INSERT INTO report (userid, number, weekstart, weekend, statusid) VALUES (?, ?, ?, ?, ?)', [$oUser->id, 2, $sNextWeekStart, $sNextWeekEnd, 1]);
        }
    }


    /**
     * Returns weekcount for a span between parameter date and active date
     *
     * @param $iStartDate
     * @return int
     */
    protected function _getWeekCount($iStartDate) : int
    {
        $startTime = strtotime($iStartDate);
        $endTime = strtotime(date('Y-m-d'));
        $cnt = 0;
        while ($startTime <= $endTime) {
            $cnt++;
            $startTime += strtotime('+1 week', 0);
        }
        return $cnt;
    }


    /**
     * Returns the selected report as Json
     *
     * @param Request $request
     * @return string
     */
    public function getSelectedReport(Request $request) : ?string
    {
        $iNumber = $request->get('report_number');
        if (Auth::check()) {
            $oUser = User::where('id', Auth::id())->first();
            $report = DB::table('report')->where('number', $iNumber)->where('userid', $oUser->id)->first();
            $report->weekstart = date('d-m-Y', strtotime($report->weekstart));
            $report->weekend = date('d-m-Y', strtotime($report->weekend));
            return \GuzzleHttp\json_encode($report);
        }
    }
}
