<?php

namespace App\Http\Controllers;

use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Report2PDF extends Controller
{
 //1234
    protected function export(Request $request)
    {
        $user = DB::table('users')->where('id', Auth::id())->first();
        $report = DB::table('report')->where('number', $request->get('report_number'))
            ->where('userid', $user->id)
            ->first();
        $pdf = new DOMPDF();
        $pdf->setPaper('A4', 'portrait');
        $pdf->loadHtml('<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Report</title>
		<style>
			body{
				margin: 5%;
			}
			.border-box{
				border: 1px solid #000;
				padding: 2px;
				margin-bottom: 3px;
			}

			.border-content{
				border: 1px solid #000;
				margin-bottom: 4px;
			}

			.box-signature{
				width: 100%;
				border: 1px solid #000;
				margin-bottom: 4px;
			}

			.box-signature-half{
				display: inline-block;
				width: 49%;
				//border-right: 1px solid #000
			}

			.date{
				padding: 6px 6px 75px;
				display: block;
			}

			.border-box-content{
				height: 180px;
				padding: 2px 5px;
			}

			.space{
			 width: 75%; border-bottom: 1px solid #000;
			}

			.border-header{
				border-bottom: 1px solid #000;
				margin: 5px;
			}

			.year{
				//margin-left: 25.6%;
                float: right;
			}

			.apprenticeship{
				display: inline-block;
				padding-bottom: 10px;
			}

			.part{
				padding-bottom: 5px
			}

			.to{
				margin-left: 15%;
			}

			.from-week{
				//margin-left: 20%;
                float: right;
			}
		</style>
	</head>
	<body>
		<div class="border-box">
			<span><b>Name, Vorname: </b>' . $user->lname . ', ' . $user->fname . '</span>
			<div>
				<span class="apprenticeship"><b>Ausbildungsnachweis Nr. </b> ' . $report->number . '
				<a class="from-week">für die Woche vom ' . date('d.m.Y', strtotime($report->weekstart)) . ' bis ' . date('d.m.Y', strtotime($report->weekend)) . '</a></span>
	        </div>
			<div class="part">
				<a>Abteilung oder Arbeitsgebiet: ' . $user->working_area . '</a>' .
            '<a class="year">Ausbildungsjahr: ' . $this->getYear($user->start_date, $report->weekstart) . '</a>
			</div>
		</div>
		<div class="border-content">
			<div>
				<h4 class="border-header">Betriebliche Tätigkeit</h4>
			</div>
			<div class="border-box-content">
				' . nl2br(htmlentities($report->content1)) . '
			</div>
		</div>
		<div class="border-content">
			<div>
				<h4 class="border-header">Unterweisung, betrieblicher Unterricht, sonstige Schulungen</h4>
			</div>
			<div class="border-box-content">
				' . nl2br(htmlentities($report->content2)) . '
			</div>
		</div>
		<div class="border-content">
			<div>
				<h4 class="border-header">Berufsschule (Unterrichtsthemen)</h4>
			</div>
			<div class="border-box-content">
				' . nl2br(htmlentities($report->content3)) . '
			</div>
		</div>
        <div class="box-signature">
            <br>
            <br>
			<div class="box-signature-half">
				<a class="date">Datum:</a>
				<div class="space"></div>
				<a>Unterschrift Auszubildener</a>
			</div>
			<div class="box-signature-half">
                <a class="date">Datum:</a>
				<div class="space"></div>
				<a>Unterschrift Ausbilder</a>
			</div>
		</div>
	</body>
</html>');
        $pdf->render();
        DB::table('report')->where('id', $report->id)->update(['statusid' => '3', 'exported' => date('Y-m-d')]);
        $pdf->stream('Wochenbericht_Nr.' . $report->number, array("Attachment" => 0));
    }

    /**
     * Checks if the User is checked in
     * @param Request $request
     */
    public function _export(Request $request)
    {
        if (Auth::check()) {
            $this->export($request);
        } else {
            $message = "Please log in to export the Report";
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
    }

    /**
     * Calculates the current Year of the Apprenticeship
     *
     * @param $sStartDate
     * @param $sWeekStart
     * @return int
     * @throws \Exception
     */
    private function getYear($sStartDate, $sWeekStart)
    {
        $oStartDate = new \DateTime($sStartDate);
        $oWeekStart = new \DateTime($sWeekStart);

        $oDiff = $oStartDate->diff($oWeekStart);
        return $oDiff->y + 1;
    }
}
