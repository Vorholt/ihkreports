<?php
/**
 * Created by IntelliJ IDEA.
 * User: lev
 * Date: 2019-01-23
 * Time: 10:48
 */

namespace App\Http\Controllers;

use Webuntis\Configuration\WebuntisConfiguration;
use Webuntis\Query\Query;

class Webuntis extends Controller
{
    protected function _init()
    {
    }


    public function test()
    {
        $oConfig = new WebuntisConfiguration([
            'default' => [
                'server' => 'asopo',
                'school' => 'BK-Ahaus',
                'username' => '',
                'password' => '',
                'path_scheme' => 'https://{server}.webuntis.com/WebUntis/jsonrpc.do?school={school}'
            ],
            'admin' => [
                'server' => 'asopo',
                'school' => 'BK-Ahaus',
                'username' => '',
                'password' => '',
                'path_scheme' => 'https://{server}.webuntis.com/WebUntis/jsonrpc.do?school={school}'
            ],
            'disable_cache' => true
        ]);
        $oQuery = new Query();
        $testData = $oQuery->get('Students')->findAll();
        return view('webuntis');
    }
}
