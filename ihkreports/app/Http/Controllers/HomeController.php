<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $oReport = new ReportController();
        $oReport->updateMissingUserReports();
        $aSidebarData = $oReport->getAllSidebarDataByUser();
        return view('home', ['aSidebarData' => $aSidebarData]);
    }
}
