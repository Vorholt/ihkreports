<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/logout', 'Auth\LogoutController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/pdfexport', 'Report2PDF@_export');
Route::post('report', 'ReportController@getSelectedReport');
Route::post('addreport', 'ReportController@store');
Route::get('refresh', 'ReportController@getAllSidebarDataByUser');
Auth::routes();
