@extends('layouts.app')
@section('body')

<body>
	<div class="Login_body">
		<div class="container">
			<div class="d-flex justify-content-center h-100">
				<div class="card_login">
					<div class="card-header">
						<h3>{{ __('Einloggen') }}</h3>
					</div>
					<div class="card-body">
						<form class="myForm" method="POST" action="{{ route('login') }}">
							@csrf
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								<input type="email" id="email"
									class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
									name="email" value="{{ old('email') }}"
									placeholder="{{ __('E-Mail Address') }}" required autofocus>
								@if($errors->has('email')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('email') }}</strong>
								</span> @endif
							</div>
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input type="password" id="password"
									class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
									name="password" required placeholder="{{ __('Passwort') }}">
								@if ($errors->has('password')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('password') }}</strong>
								</span> @endif
							</div>
							<div class="remember">
								<input class="remember input" type="checkbox" name="remember"
									id="remember"{{old('remember') ? 'checked' : '' }}>

								<label class="form-check-label" for="remember"> {{ __('Remember
									Me') }} </label>
							</div>
							<div class="input-group form-group">
							<!--  class="login_btn btn-lg btn-block" -->
								<button class="btn-lg btn-block login_btn" type="submit">{{ __('Login') }}</button>

								<!-- 							@if (Route::has('password.request')) <a class="btn btn-link" -->
								<!-- 								href="{{ route('password.request') }}"> {{ __('Forgot Your -->
								<!-- 								Password?') }} </a> @endif -->
							</div>
						</form>
					</div>
					<div class="card-footer">
						<div class="d-flex justify-content-center links">
							{{__('Keinen Account?') }} <a href="{{route('register')}}">{{__('Sign
								up') }}!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
@endsection
