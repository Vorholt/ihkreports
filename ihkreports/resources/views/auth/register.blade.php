@extends('layouts.app')
@section('body')
<body>
	<div class="register_body">
		<div class="container">
			<div class="d-flex justify-content-center h-100">
				<div class="card_register">
					<div class="card-header">
						<h3>{{ __('Registrieren') }}</h3>
					</div>
					<div class="card-body">
						<form class="myForm" method="POST"
							action="{{ route('register') }}">
							@csrf


							<!-- ==== FIRSTNAME ==== -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								<input type="text" id="fname"
									class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}"
									name="fname" value="{{ old('fname') }}"
									placeholder="{{ __('Vorname') }}" required autofocus>
								@if($errors->has('fname')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('fname') }}</strong>
								</span> @endif
							</div>
							<!-- ==== END FIRSTNAME ==== -->

                            <!-- ==== SIRNAME ==== -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								<input type="text" id="lname"
									class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}"
									name="lname" value="{{ old('lname') }}"
									placeholder="{{ __('Nachname') }}" required autofocus>
								@if($errors->has('lname')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('lname') }}</strong>
								</span> @endif
							</div>
                            <!-- ==== END SIRNAME ==== -->

                            <!-- ==== EMAIL ==== -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-at"></i></span>
								</div>
								<input type="email" id="email"
									class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
									name="email" value="{{ old('email') }}"
									placeholder="{{ __('Email - Adresse') }}" required autofocus>
								@if($errors->has('email')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('email') }}</strong>
								</span> @endif
							</div>
							<!-- ==== END EMAIL ==== -->

							<!-- ==== START DATE ==== -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
								</div>
								<input type="date" id="start_date"
									class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }}"
									name="start_date"  value="{{ old('start_date') }}" required placeholder="{{ __('Ausbildungsbeginn') }}">
								@if ($errors->has('start_date')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('start_date') }}</strong>
								</span> @endif
							</div>
							<!-- ==== END START DATE ==== -->

							<!-- ==== COMPANY ==== -->
                            <div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-building"></i></span>
								</div>
								<input type="text" id="company"
									class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}"
									name="company" value="{{ old('company') }}" required placeholder="{{ __('Ausbildungsbetrieb') }}">
								@if ($errors->has('company')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('company') }}</strong>
								</span> @endif
							</div>
                            <!-- ==== END COMPANY ==== -->
                            
                            <!-- ==== WORKING AREA ==== -->
                            <div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-building"></i></span>
								</div>
								<input type="text" id="working_area"
									class="form-control{{ $errors->has('working_area') ? ' is-invalid' : '' }}"
									name="working_area" value="{{ old('working_area') }}" required placeholder="{{ __('Ausbildungsabteilung') }}">
								@if ($errors->has('working_area')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('working_area') }}</strong>
								</span> @endif
							</div>
                            <!-- ==== END WORKING AREA==== -->
                            
                            <!-- ==== PASSWORD ==== -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input type="password" id="password"
									class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
									name="password" required placeholder="{{ __('Passwort') }}">
								@if ($errors->has('password')) <span class="invalid-feedback"
									role="alert"> <strong>{{ $errors->first('password') }}</strong>
								</span> @endif
							</div>
							<!-- ==== END PASSWORD ==== -->
							
							<!-- ==== CONFIRM PASSWORD ==== -->
							<div class="input-group form-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input type="password" id="password_confirmation"
									class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
									name="password_confirmation" required
									placeholder="{{ __('Passwort wiederholen') }}">
								@if($errors->has('password_confirmation')) <span
									class="invalid-feedback" role="alert"> <strong>
										{{$errors->first('password_confirmation') }} </strong>
								</span> @endif
							</div>
							<!-- ==== END CONFIRM PASSWORD ==== -->
							
							<!-- ==== REGISTER ==== -->
							<button class="login_btn btn-lg btn-block login_btn"
								type="submit">{{ __('Register') }}</button>
							<!-- ==== END REGISTER ==== -->
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
@endsection
