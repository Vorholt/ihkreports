@if (Route::has('login'))
<div class="top-right links">
    @auth
    <script>window.location = "{{ route('home') }}";</script>
    <a href="{{ route('home') }}">Home</a>
    @else
    <script>window.location = "{{ route('login') }}";</script>
    <a href="{{ route('login') }}">Login</a>
    @endauth
</div>
@endif
