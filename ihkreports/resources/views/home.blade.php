@extends('layouts.app')

@section('body')
<body>
    <!-- Header -->
    @include('partials/header')
    <div id="app">
        <!-- /Header -->
        <dashboard-component :reports="{{$aSidebarData}}" :user="{{Auth::User()}}"></dashboard-component>
    </div>
<script src="{{asset('js/app.min.js')}}"></script>
</body>
@endsection
