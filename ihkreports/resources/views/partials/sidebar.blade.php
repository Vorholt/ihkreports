<div class="sidebar-sticky" style="width: 200px">
    <ul class="nav flex-column bg-dark">
        @foreach ($aSidebarData as $data)
        <li class="nav-item">
            <a class="nav-link" href="#" id="{{$data['id']}}">{{$data['nr']}}</a>
        </li>
        @endforeach
    </ul>
</div>
