<nav class="navbar sticky-top navbar-dark bg-dark">
	<span class="navbar-brand mb-0 h1">IHK Reports</span>
	<!--<a class="btn btn_logout" href="{{ url('/logout') }}"> {{__('Logout')}} </a>-->

	<span class="nav-item dropdown">
	<a class="nav-link dropdown-toggle"
		id="navbarDropdownMenuLink-333" data-toggle="dropdown"
		aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user fa-2x"></i>
	</a>
		<span class="dropdown-menu dropdown-menu-right dropdown-default"
			aria-labelledby="navbarDropdownMenuLink-333">
			<a class="dropdown-item" href="{{ url('/logout') }}">{{__('Logout')}}</a>
		</span>
	</span>
</nav>
