# Settings for the docker environment

1. Before you start the docker containers, create a file named ".env" with the following content (replace with your IP address):
    1. ```XDEBUG_HOST_IP=172.30.215.137```

# Docker commands
```
   # start docker containers
   docker-compose up -d
   # stop docker containers
   docker-compose down
   # show all running containers
   docker ps -a
   # access docker container
   docker exec -it ihkreports bash
```
